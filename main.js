const personsBox = document.getElementById("characters__box"); //On récupère l'élément ayant cet id
const arrayPersons = document.createElement('ul'); //On crée dans le DOM un ul
arrayPersons.classList.add('d-flex','flex-column','persons__box'); //On donne des classes à l'ul
personsBox.append(arrayPersons); //On ajout à la section#characters__box l'ul qu'on vient de créer

async function persons() { //On crée une fonction asynchrone
    const response = await fetch("https://jsonplaceholder.typicode.com/users"); //On récupère le fichier JSON suivant
    const personsList = await response.json(); //On analyse le contenu du fichier json et on stocke ce contenu

    for(const person of personsList){ //On boucle dans le tableau pour chaque objet
        const liPerson = document.createElement('li'); //On crée un li
        liPerson.classList.add('card', 'person__card'); //Le li prend les classes suivantes

        const personName = document.createElement('span'); //On crée un span pour stocker le nom de la personne
        personName.style.fontWeight = "bold"; //On lui affecte du style
        personName.append(person.name); //Ce span contiendra la valeur de la propriété name de l'objet

        const personPresentation = document.createElement('p'); //On crée un p pour stocker le nom et le nom d'utilisateur
        personPresentation.append(personName, " - ", person.username);//On ajoute au p les valeurs des propriétés name et username de l'objet
        liPerson.append(personPresentation); //On ajoute au li ce p

        const street = document.createElement('p'); //On crée un p pour y stocker la rue
        street.append(person.address.street); //On ajoute au p la valeur de street contenu dans l'objet address lui même contenu dans l'objet person

        const city = document.createElement('p'); //On crée un p
        city.append(person.address.city, ", " ,person.address.zipcode) //On ajoute au p la valeur de city contenu dans l'objet address, on ajoute aussi une chaîne de caractère pour séparer visuellement les deux valeurs récupérées

        const detailsAdress = document.createElement('div'); //On crée une div
        detailsAdress.append(street, city); //On y ajoute les variables street et city
        detailsAdress.classList.add('details__adress'); //On lui donne une classe

        liPerson.append(detailsAdress); //On ajoute au li la div 
        arrayPersons.append(liPerson); //On ajoute à l'ul le li
    }
}

persons();